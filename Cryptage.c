#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>

#include "Caracteres.h"


void MessageToCesar(wchar_t *locMessage, int CleCesar, wchar_t *locAlphabet) {

    // position dans l'alphabet du caractere crypté
    int val;
    // parcours du message
    for (int i = 0; i < wcslen(locMessage); i++) {

        // si le caractere n'est pas un espace faire
        if (EstEspace(locMessage[i]) != 1) {
            // position dans l'alphabet du caractere du message
            int j = 0;
            // recherche dans l'alphabet du caractere correspondant
            while(locAlphabet[j] != locMessage[i]) {
                j++;
            }
            // réalisation du décalage graces à l'indices du caracteres et à la clé
            val = (j + CleCesar) % wcslen(locAlphabet);
            // affectation de la lettre correspondante à val au niveau de l'indice du message
            locMessage[i] = locAlphabet[val];
            // fin de cryptage du caractere
        }
        // fin de parcours du message
    }
    // fin de la fonction
}

void CesarToMessage(wchar_t *locMessage, int CleCesar, wchar_t *locAlphabet) {

    // position dans l'alphabet du caractere décrypté
    int val;
    // parcours du message
    for (int i = 0; i < wcslen(locMessage); i++) {

        // si le caractere n'est pas un espace faire
        if (EstEspace(locMessage[i]) != 1) {
            // position dans l'alphabet du caractere du message
            int j = 0;
            // recherche dans l'alphabet du caractere correspondant
            while(locAlphabet[j] != locMessage[i]) {
                j++;
            }
            // réalisation du décalage graces à l'indices du caracteres et à la clé
            val = (j - CleCesar);
            // modification en cas de dépassement de la taille de l'alphabet
            if (val > wcslen(locAlphabet)) {
                val = val + wcslen(locAlphabet);
            }
            // affectation de la lettre correspondante à val au niveau de l'indice du message
            locMessage[i] = locAlphabet[val]; 
            // fin de cryptage du caractere
        }
        // fin de parcours du message
    }
    // fin de la fonction
}

void MessageToVigenere(wchar_t *locMessage, wchar_t *locCleVigenere, wchar_t *locAlphabet) {

    // correspondance des caracteres de la clé dans l'alphabet (indexClé)
    int indiceCle[wcslen(locCleVigenere)]; 
    // position dans la clé
    int k = 0; 
    // position dans l'alphabet du caractere crypté
    int val;

    // parcours de la clé
    for (int i = 0; i < wcslen(locCleVigenere); ++i) {
        // position dans l'alphabet du caractere du message
        int j = 0;
        // recherche dans l'alphabet du caractere correspondant
        while(locAlphabet[j] != locCleVigenere[i]) {
            j++;
        }
        // affectation de l'indice de position dans l'alphabet à l'indice de position dans la clé
        indiceCle[i] = j;
        // fin de remplissage de l'indexClé
    }

    // parcours du message
    for (int i = 0; i < wcslen(locMessage); i++) {

        // si le caractere n'est pas un espace faire
        if (EstEspace(locMessage[i]) != 1) {
            // position dans l'alphabet du caractere du message
            int j = 0; 
            // recherche dans l'alphabet du caractere correspondant
            while(locAlphabet[j] != locMessage[i]) {
                j++;
            }
            // réalisation du décalage graces aux indices des caracteres et cryptage
            val = (j + indiceCle[k]) % wcslen(locAlphabet);
            // affectation de la lettre correspondante à val au niveau de l'indice du message
            locMessage[i] = locAlphabet[val];
            // incrementation de la position dans la clé
            k = (k + 1) % wcslen(locCleVigenere);
            // fin de cryptage du caractere
        }
        // fin de parcours du message
    }
    // fin de la fonction
}
    

void VigenereToMessage(wchar_t *locMessage, wchar_t *locCleVigenere, wchar_t *locAlphabet) {

    // correspondance des caracteres de la clé dans l'alphabet (indexClé)
    int indiceCle[wcslen(locCleVigenere)]; 
    // position dans la clé
    int k = 0; 
    // position dans l'alphabet du caractere crypté
    int val;

    // parcours de la clé
    for (int i = 0; i < wcslen(locCleVigenere); ++i) {
        // position dans l'alphabet du caractere du message
        int j = 0;
        // recherche dans l'alphabet du caractere correspondant
        while(locAlphabet[j] != locCleVigenere[i]) {
            j++;
        }
        // affectation de l'indice de position dans l'alphabet à l'indice de position dans la clé
        indiceCle[i] = j;
        // fin de remplissage de l'indexClé
    }

    // parcours du message
    for (int i = 0; i < wcslen(locMessage); i++) {

        // si le caractere n'est pas un espace faire
        if (EstEspace(locMessage[i]) != 1) {
            // position dans l'alphabet du caractere du message
            int j = 0; 
            // recherche dans l'alphabet du caractere correspondant
            while(locAlphabet[j] != locMessage[i]) {
                j++;
            }
            // réalisation du décalage graces aux indices des caracteres et cryptage
            val = (j - indiceCle[k]);
            // modification en cas de dépassement de la taille de l'alphabet
            if (val > wcslen(locAlphabet)) {
                val = val + wcslen(locAlphabet);
            }
            // affectation de la lettre correspondante à val au niveau de l'indice du message
            locMessage[i] = locAlphabet[val];
            // incrementation de la position dans la clé
            k = (k + 1) % wcslen(locCleVigenere);
            // fin de cryptage du caractere
        }
        // fin de parcours du message
    }
    // fin de la fonction
}
