#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>

#include "Utilisateur.h"
#include "Caracteres.h"
#include "Cryptage.h"

int main(void) {

				/*=====DEFINITION VARIABLES=====*/

	//Definition d'utilisation de l'encodage local des caracteres
	struct lconv *loc;
	setlocale(LC_ALL, "");
	loc = localeconv();

	//Definition variable du choix de l'algorithme & du chiffrage/dechiffrage
	int ChoixAlgo;
	int *locChoixAlgo = &ChoixAlgo;


	//Definition de l'alphabet
	wchar_t Alphabet[26] = {L"abcdefghijklmnopqrstuvwxyz"};

	//Definition de la variable contenant le message
	wchar_t message[500];
	//Definition clé pour Cesar
	int CleCesar;
	int *locCleCesar = &CleCesar;

	//Definition clé pour vigenère
	wchar_t CleVigenere[100];

			/*=====PROGRAMME PRINCIPAL=====*/

	//Sélection du cryptage à réaliser
	InputChoixCryptage(locChoixAlgo);

	do {
		//Demande du message à chiffrer à l'utilisateur
		InputMessage(message);

		//Conversion du message
		ConvertChar(message);
		//vérification caractères interdits
		if (CheckChar(message) == 1 ) {
			wprintf(L"\nERREUR: Caractere(s) incorrecte(s) dans le message\nSortie du programme, veuillez recommencer.\n\n");
		}
	// redemande le message si erreur dans l'entrée
	} while (CheckChar(message) == 1);

	//chiffrement cesar
	if (ChoixAlgo == 0) {
		//Demande de la clé César
		InputCleCesar(&CleCesar, Alphabet);
		//Modification du message
		MessageToCesar(message, CleCesar, Alphabet);
	}

	//dechiffrement cesar
	if (ChoixAlgo == 1) {
		//Demande de la clé César
		InputCleCesar(locCleCesar, Alphabet);
		//Modification du message
		CesarToMessage(message, CleCesar, Alphabet);
	}

	//chiffrement vigenere
	if (ChoixAlgo == 2) {
		//Demande de la clé Vigenere
		InputCleVigenere(CleVigenere);
		ConvertChar(CleVigenere);
		if (CheckCleVigenere(CleVigenere, Alphabet) == 1 ) {
		wprintf(L"\nERREUR: Espace(s) dans la Clé\nSortie du programme, veuillez recommencer.\n\n");
		//quitte le programme
		}else{
		//Modification du message
		MessageToVigenere(message, CleVigenere, Alphabet);
		}

	}

	//dechiffrement vigenere
	if (ChoixAlgo == 3) {
		//Demande de la clé Vigenere
		InputCleVigenere(CleVigenere);
		ConvertChar(CleVigenere);
		if (CheckCleVigenere(CleVigenere, Alphabet) == 1 ) {
		wprintf(L"\nERREUR: Espace(s) dans la Clé\nSortie du programme, veuillez recommencer.\n\n");
		//quitte le programme
		}else{			
		//Modification du message
		VigenereToMessage(message, CleVigenere, Alphabet);
		}
	}

	//Affichage du résultat et enregistrement dans un fchier texte
	OutputResultat(message);
			
	//fin du programme
}
