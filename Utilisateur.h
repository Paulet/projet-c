// fonction permettant d'enregistrer le choix de chiffrement que veut realiser l'utilisateur
void InputChoixCryptage(int *locChoixAlgo);
// fonction permmetant d'enregistrer le message de l'utilisateur dans un tableau
void InputMessage(wchar_t *locMessage);
// fonction permettant d'enregistrer la cle cesar de l'utilisateur
void InputCleCesar(int *locCleCesar, wchar_t *locAlphabet);
// fonction permettant d'enregistrer la cle vigenere de l'utilisateur
void InputCleVigenere(wchar_t *locCleVigenere);
// Enregistre les messages chiffré/déchiffré dans un fichier texte
void OutputResultat(wchar_t *locMessage);