#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>

#include "Utilisateur.h"
#include "Caracteres.h"

// fonction pour vider la memoire lors de saisies utilisateurs
void viderBuffer() {
    int c = 0;
    do {
		c = getwchar();
	} while (c != '\n' && c != EOF);
}

// saisie du choix tant que la valeur entrée n'est pas bonne
int SaisieChoix(int min, int max) {

	int choix;
	while (1) {
		if (wscanf(L"%d", &choix) == 0) {
			viderBuffer();
		} else if (min <= choix && choix <= max) {
			viderBuffer();
			return choix;
		}
		wprintf(L"Veuillez saisir une valeur valide : ");
	}
}

// menu du programme
void InputChoixCryptage(int *locChoixAlgo) {

	wprintf(L"Choisissez le type de chiffrement :\n\n");
	wprintf(L"Code de César\n");
	wprintf(L"0 - Chiffrer\n");
	wprintf(L"1 - Déchiffrer\n\n");
	wprintf(L"Code de Vigenère\n");
	wprintf(L"2 - Chiffrer\n");
	wprintf(L"3 - Déchiffrer\n\n");

	wprintf(L"Saisissez votre choix : ");
	*locChoixAlgo = SaisieChoix(0, 3);
}

void InputMessage(wchar_t *locMessage) {

	wprintf(L"Entrez le message à convertir (max 500 caracères) : \n");
	// message de 500 caracteres max
	fgetws(locMessage, 500, stdin);
}

void InputCleCesar(int *locCleCesar, wchar_t *locAlphabet) {
	wprintf(L"Entrez la clé de César (entre 0 et %d) : ", wcslen(locAlphabet));
	// cle cesar comprise en 0 et la taille de l'alphabet
	*locCleCesar = SaisieChoix(0, wcslen(locAlphabet));
	wprintf(L"\n");
}


void InputCleVigenere(wchar_t *locCleVignere) {
	wprintf(L"Entrez la clé de Vigenère (suite de max 100 caracteres sans espaces): \n");
	// cle vigenere de 100 caracteres max
	fgetws(locCleVignere, 100, stdin);
}

void OutputResultat(wchar_t *locMessage) {

	// affichage des la variable "message" une fois crypté dans un fichier texte
	wprintf(L"Voici le résultat : \n%ls\n", locMessage);
	FILE *fp;
	// les messages precedents sont conservé
	// et les nouveaux sont ecrit a la suite
	fp = fopen("resultat.txt", "a"); 
	if (fp)	{
		fputws(locMessage , fp);
    	fputws(L"\n", fp);
    	// affichage dans le terminal
		wprintf(L"Ce message est sauvegardé dans le fichier resultat.txt \n");
	}else{
		wprintf(L"Erreur à l'ouverture du fichier de sauvegarde\n");
	}
	fclose(fp);
}