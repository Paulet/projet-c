#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>

#include "Caracteres.h"


void ConvertChar(wchar_t *locMessage) {

	int i = 0;

	// Effacage du "\n" à la fin du tableau
	locMessage[wcslen(locMessage)-1] = '\0';

	// Conversion des caracteres speciaux

  	while( i < wcslen(locMessage)) { 

        //Conversion des majuscules en minuscules
		locMessage[i] = tolower(locMessage[i]);

		switch (locMessage[i]) {
    		case L'à':
    		case L'À':
    		case L'â':
    		case L'Â':
    		case L'ä':
    		case L'Ä':
    			locMessage[i] = L'a';
    			break;
    		case L'é':
    		case L'É':
    		case L'è':
    		case L'È':
    		case L'ê':
    		case L'Ê':
    		case L'ë':
    		case L'Ë':
    			locMessage[i] = L'e';
    			break;
    		case L'ï':
    		case L'Ï':
    		case L'î':
    		case L'Î':
    			locMessage[i] = L'i';
    			break;
    		case L'ô':
    		case L'Ô':
    		case L'ö':
    		case L'Ö':
    			locMessage[i] = L'o';
    			break;
    		case L'ù':
    		case L'Ù':
    		case L'û':
    		case L'Û':
    		case L'ü':
    		case L'Ü':
    			locMessage[i] = L'u';
    			break;
    		case L'ÿ':
    			locMessage[i] = L'y';
    			break;
    		case L'ç':
    			locMessage[i] = L'c';
    			break; 	
    		default:
    			break;
		}
        i++;
	}
}

int EstEspace(wchar_t Valeur) {
	if (Valeur == ' ') {
		return 1;
	}else{
		return 0;
	}
}

//cette fonction n'est pas opti pour tous les alphabet en entrée
//et elle est très moche
int CheckChar(wchar_t *locMessage) {
    
    int i = 0;
    while( i < wcslen(locMessage)) {

      switch (locMessage[i]) {
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
        case ' ':
            break;
        default:
            return 1;// cas d'erreur ou un caractere n'appartient pas à l'alphabet
            break;
        }
        i++;
    }
  	return 0;  
}


// meme commentaire que la fonction precedente  °_°
// locAlphabet ne sert pas dans cette version moche mais serait util dans ne version optimiser
int CheckCleVigenere(wchar_t *locCleVigenere, wchar_t *locAlphabet) {
    
    int i = 0;
    while(i < wcslen(locCleVigenere)) {

        switch (locCleVigenere[i] ) {
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
            //case ' ':
                break;
            default:
                return 1;// cas d'erreur ou un caractere n'appartient pas à l'alphabet
                break;
        }
        i++;
    }
  	return 0; 
}