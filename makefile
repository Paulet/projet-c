SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c, %.o, ${SOURCES})
GCC = gcc

all:  Utilisateur.o Caracteres.o Cryptage.o main

main: ${BINAIRES}
	${GCC} $^ -o $@

%.o: %.c %.h
	${GCC} -c $<

clear:
	rm *.o
	rm main