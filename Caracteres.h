// fonction pour convertir les caractères spéciaux
void ConvertChar(wchar_t *locMessage);
// fonction pour vérifier si les caractères appartiennent a l'alphabet dans le message en entée
int CheckChar(wchar_t *locMessage);
// fonction qui renvoie 1 si le caractere en entrée est un espace
int EstEspace(wchar_t Valeur);
// fonction pour vérifier si les caractères appartiennent a l'alphabet das la cle de vigenere
int CheckCleVigenere(wchar_t *locMessage, wchar_t *locAlphabet);