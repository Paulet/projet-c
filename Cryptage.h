// Chiffrement de cesar
void MessageToCesar(wchar_t *locMessage, int CleCesar, wchar_t *locAlphabet);
// Déchiffrement de cesar
void CesarToMessage(wchar_t *locMessage, int CleCesar, wchar_t *locAlphabet);
// Chiffrement de vigenere
void MessageToVigenere(wchar_t *locMessage, wchar_t *locCleVigenere, wchar_t *locAlphabet);
// Dechiffrement d vigenere
void VigenereToMessage(wchar_t *locMessage, wchar_t *locCleVigenere, wchar_t *locAlphabet);