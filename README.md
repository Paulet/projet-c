Projet en langage C : sujet 4 : chiffrement de messages

Membres groupe : Paulet Sylvain et Laborde Corentin

Accès Git : https://framagit.org/Paulet/projet-c
fichiers suivants :

    sujet
    
    makefile
    main.c
    Caracteres.h
    Caracteres.c
    Cryptage.h
    Cryptage.c
    Utilisateur.h
    Utilisateur.c
    projet_C.zip
    
    
coder avec SublimeText3 et VisualStudioCode

tests réaliser sur : https://repl.it/languages/c